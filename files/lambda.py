#!/usr/bin/env python3
# Make sure we have python3 here.... (xrange vs range)
from __future__ import print_function

import boto3
import logging
import json
import os
from pprint import pformat, pprint
from math import ceil, floor
import random
import re
import sys
import time


'''
ECS Monitor

This Lambda function triggers on Cloudwatch Event Pattern events to create, update, delete cloudwatch metrics to keep track of ECS issues.
Two core issues are tackled here:
1. ECS Agent disconnected (due to i.e. EC2 machine being ok but docker died or the ecsagent task)                 -- cluster wide check
2. Service not starting properly (i.e. due to resource shortage -> one task can start but not the desired amount) -- per service check

Alternatively this function can be scheduled through a Cloudwatch Schedule so it runs every 5 minutes (or even every minute).
In this mode it will scan -all- clusters (except for ExcludeClusters) and check the checks for all instances and underlying services (except for ExcludeServices).

Concept:
1. ECS Agent state
A Cloudwatch event pattern is created that triggers on EC2 Instance state changes, this will include the agent state.
Whenever it is disconnected this will create an alarm in cloudwatch called $clustername-ECSAgent.
If the instance reconnects the alarm will be disabled again.
^-- how to handle termination / cluster delete?

2. ECS Service state
A cloudwatch event pattern is created that triggers on ECS Task state changes. Every time this lambda runs it will update the list of services and alarms.
Every service get an alarm called $service-scale, which will go to alarm state when desired does not match running tasks.
If a task does not belong to a service it is ignored.

(3. TODO - ECS Task state - alert on failed tasks that do not belong to a service and/or match a specific name regex)

Metrics are stored under the ECSMonitor namespace:
ClusterName | Agent_Connected      - should be non-0, assuming you have instances in your cluster
ClusterName | Agent_Disconnected   - should be 0, in case it goes up for a longer period than a minute your instance(s) probably have docker issues
ClusterName | Tasks_Running        - should probably be non-0 in an active cluster
ClusterName | Tasks_Pending        - should be near 0, in case it stays up for a long period you have too little capacity in your cluster or invalid taskdefinitions
ClusterName | Services_Active      - Number of active services, statistical only.
ClusterName | clusterInstanceScale - Should be 0, used for TargetTrackingScaling on EC2 ASG. When positive more instances are needed, negative means we have too many. See ScaleBias.

This lambda is influenced by the following ENVIRONMENT variables (usually set when deploying the lambda):
===
LogLevel       : changes the loglevel, default is INFO, other options are DEBUG, WARNING, ERROR
IncludeClusters: cluster names regex (comma seperated) to include, i.e. "^prd-.*,.*-acc-.*" -- Using Include means only matching clusters will be checked --TODO--
IncludeServices: service names regex (comma seperated) to include, i.e. "-tst-,^acc-" -- Using Include means only matching services will be checked --TODO--
ExcludeClusters: cluster names regex (comma seperated) to skip, i.e. "-tst$,^acc-" --TODO--
ExcludeServices: service names regex (comma seperated) to skip, i.e. "-tst-,^acc-" --TODO--
ScaleMetrics   : Set to True to enable the calculation of schedulable container metrics. Needs parsing of taskdefinitions etc, so more api calls and slower.
ScaleBias      : Defaults to 0 - reserve spare capacity for X amount of times that the largest container can be placed.
ScaleBiasCPU   : Defaults to 0 - reserve X spare CPU capacity. This is added to ScaleBias.
ScaleBiasMEM   : Defaults to 0 - reserve X spare MEM capacity. (AND-ed with CPU). This is added to ScaleBias.
===

Regarding Include/Exclude options: they are optional, but when specified:
- Include means other cluster/services will be ignored completely as far as possible
- Exclude means that if Include (or the lack thereof) matched them they will still be filtered out.
'''


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def rdebug(o):
    retype = type(re.compile('foo'))
    if o is None:
        return "None"
    elif type(o) == retype:
        return '/' + o.pattern + '/'
    elif isinstance(o, list):
        return "[" + ",".join(rdebug(i) for i in o) + "]"
    else:
        return pformat(o)


class ECSMonitor:
    def __init__(self):
        self.ecs = boto3.client('ecs')
        self.cloudwatch = boto3.client('cloudwatch')
        self.clusters = {}
        self.taskdefs = {}
        self.metrics = []
        self.scaleMetrics = False

    def configure_logger(self, loglevel=logging.INFO, logfile=None):
        logger = logging.getLogger()
        logger.setLevel(loglevel)
        format = "[%(filename)30s:%(funcName)20s():%(lineno)4s:%(levelname)-7s] %(message)s"
        if logfile and logfile is not None:
            logging.basicConfig(level=loglevel,
                                filename=logfile,
                                filemode='w',
                                format=format)
        else:
            logging.basicConfig(level=loglevel,
                                format=format)
        logging.getLogger('boto3').setLevel(logging.WARNING)
        logging.getLogger('botocore').setLevel(logging.WARNING)

    def setInclusions(self, includeClusters, includeServices, excludeClusters, excludeServices):
        self.excludeClusters = excludeClusters
        self.excludeServices = excludeServices
        self.includeClusters = includeClusters
        self.includeServices = includeServices
        if self.excludeClusters is not None:
            if self.excludeClusters.strip():
                regsrcs = self.excludeClusters.strip().split(',')
                self.excludeClusters = []
                for reg in regsrcs:
                    self.excludeClusters += [re.compile(reg)]
            else:
                self.excludeClusters = None
        if self.excludeServices is not None:
            if self.excludeServices.strip():
                regsrcs = self.excludeServices.split(',')
                self.excludeServices = []
                for reg in regsrcs:
                    self.excludeServices += [re.compile(reg)]
            else:
                self.excludeServices = None
        if self.includeClusters is not None:
            if self.includeClusters.strip():
                regsrcs = self.includeClusters.split(',')
                self.includeClusters = []
                for reg in regsrcs:
                    self.includeClusters += [re.compile(reg)]
            else:
                self.includeClusters = None
        if self.includeServices is not None:
            if self.includeServices.strip():
                regsrcs = self.includeServices.split(',')
                self.includeServices = []
                for reg in regsrcs:
                    self.includeServices += [re.compile(reg)]
            else:
                self.includeServices = None
        logging.debug("Configured includes: Clusters[%s] Services[%s]\nConfigured excludes: Clusters[%s] Services[%s]",
                      rdebug(self.includeClusters), rdebug(self.includeServices), rdebug(self.excludeClusters), rdebug(self.excludeServices))

    def getEnv(self, name, default):
        if name in os.environ:
            return os.environ[name]
        else:
            return default

    def error(self, logmsg):
        logging.error(logmsg)
        self.result['msg'] = logmsg
        self.result['success'] = 0
        return False

    def getTaskDefinition(self, taskdefArn):
        if taskdefArn in self.taskdefs:
            return self.taskdefs[taskdefArn]
        try:
            logging.debug("Fetching taskdefinition for %s", taskdefArn)
            self.taskdefs[taskdefArn] = self.ecs.describe_task_definition(
                taskDefinition=taskdefArn,
            )['taskDefinition']
        except Exception as e:
            return self.error("Error fetching taskdefinition {} - {}".format(taskdefArn, pformat(e)))
        return self.taskdefs[taskdefArn]

    def scanClusters(self):
        # Scan clusters and their container instances
        paginator = self.ecs.get_paginator('list_clusters')
        clusterArns = []
        try:
            clusterArns = paginator.paginate().build_full_result()['clusterArns']
        except Exception as e:
            return self.error("Error fetching taskdefinitions - {}".format(pformat(e)))

        logging.debug("Got %s clusters, fetching their details....", len(clusterArns))
        # Fetch cluster details in chunks of max 100 due to api limits
        self.clusters = {}
        for arns in chunker(clusterArns, 100):
            response = self.ecs.describe_clusters(
                clusters=arns,
                include=[
                    'STATISTICS',
                ]
            )
            for cluster in response['clusters']:
                if self.includeClusters is not None:
                    matched = False
                    for reg in self.includeClusters:
                        if reg.search(cluster['clusterName']):
                            matched = True
                            break
                    if not matched:
                        logging.debug("Skipping cluster %s since it does not match supplied includeCluster regexes", cluster['clusterName'])
                        continue
                if self.excludeClusters is not None:
                    matched = False
                    for reg in self.excludeClusters:
                        if reg.search(cluster['clusterName']):
                            # continue 'clusterlabel' -- python does not do cool stuff like perl does ;)
                            matched = True
                            break
                    if matched:
                        logging.debug("Skipping cluster %s since it matched an excludeCluster regex", cluster['clusterName'])
                        continue
                logging.info("Cluster %30.30s [%s]: %s instances, tasks %s active | %s pending, %s services", cluster['clusterName'], cluster['status'], cluster['registeredContainerInstancesCount'], cluster['runningTasksCount'], cluster['pendingTasksCount'], cluster['activeServicesCount'])
                self.clusters[cluster['clusterName']] = cluster
        '''
            'clusters': [{
                'clusterArn': 'arn:aws:ecs:eu-west-1:123412341006:cluster/example-app-server-ecs',
                'clusterName': 'example-app-server-ecs',
                'status': 'ACTIVE',
                'registeredContainerInstancesCount': 0,
                'runningTasksCount': 0,
                'pendingTasksCount': 0,
                'activeServicesCount': 1,
                'statistics': [{
                    'name': 'runningEC2TasksCount', 'value': '0'}, {
                    'name': 'runningFargateTasksCount', 'value': '0'}, {
                    'name': 'pendingEC2TasksCount', 'value': '0'}, {
                    'name': 'pendingFargateTasksCount', 'value': '0'}, {
                    'name': 'activeEC2ServiceCount', 'value': '1'}, {
                    'name': 'activeFargateServiceCount', 'value': '0'}, {
                    'name': 'drainingEC2ServiceCount', 'value': '0'}, {
                    'name': 'drainingFargateServiceCount', 'value': '0'}]
            }],
            'failures': []
        '''
        for cluster in self.clusters:
            self.updateMetrics('Tasks_Running', self.clusters[cluster]['runningTasksCount'], [{'Name': 'ClusterName', 'Value': cluster}])
            self.updateMetrics('Tasks_Pending', self.clusters[cluster]['pendingTasksCount'], [{'Name': 'ClusterName', 'Value': cluster}])
            self.updateMetrics('Services_Active', self.clusters[cluster]['activeServicesCount'], [{'Name': 'ClusterName', 'Value': cluster}])
            self.scanClusterInstances(cluster)
            self.scanServices(cluster)
            if self.scaleMetrics:
                self.updateClusterScaleMetrics(cluster)

    def scanClusterInstances(self, clusterName):
        paginator = self.ecs.get_paginator('list_container_instances')
        instance_list = []
        try:
            instance_list = paginator.paginate(cluster=clusterName).build_full_result()['containerInstanceArns']
        except Exception as e:
            return self.error("Error fetching list of container instances - {}".format(pformat(e)))

        # Next, describe them so we know their state AND stats.
        connected = []
        disconnected = []
        draining = []
        self.clusters[clusterName]['instances'] = {}
        for arns in chunker(instance_list, 100):
            response = self.ecs.describe_container_instances(
                cluster=clusterName,
                containerInstances=arns
            )
            # See https://boto3.readthedocs.io/en/latest/reference/services/ecs.html#ECS.Client.describe_container_instances for the huge list of info we have now
            for instance in response['containerInstances']:
                logging.info("Cluster [%30.30s] Instance[%10.10s] Status[%s] Agent Connected[%s]", clusterName, instance['ec2InstanceId'], instance['status'], instance['agentConnected'])
                # If instance is ACTIVE and agentConnected not True we want to alert
                if instance['status'] == 'ACTIVE':
                    if instance['agentConnected'] is True:
                        connected += [instance['ec2InstanceId']]
                    else:
                        disconnected += [instance['ec2InstanceId']]
                elif instance['status'] == 'DRAINING':
                    draining += [instance['ec2InstanceId']]
                else:
                    logging.warning("Huh, weird instance status: %s -- ignoring", instance['status'])
                # Register instance in our cluster hash
                self.clusters[clusterName]['instances'][instance['ec2InstanceId']] = instance

            self.updateMetrics('Agent_Disconnected', len(disconnected), [{'Name': 'ClusterName', 'Value': clusterName}])
            self.updateMetrics('Agent_Connected', len(connected), [{'Name': 'ClusterName', 'Value': clusterName}])
            if len(disconnected) > 0:
                logging.warn("WARNING: %s Disconnected agents detected on cluster %s!" % (len(disconnected), clusterName))
        # Also write this if we have no instances.
        if (len(connected) + len(disconnected)) == 0:
            self.updateMetrics('Agent_Disconnected', len(disconnected), [{'Name': 'ClusterName', 'Value': clusterName}])
            self.updateMetrics('Agent_Connected', len(connected), [{'Name': 'ClusterName', 'Value': clusterName}])

    def updateMetrics(self, metric, value, dimensions):
        # Create metric if it does not exist yet, i.e.
        logging.debug("Update delayed metrics %s [%s] with value %s" % (metric, pformat(dimensions), value))
        self.metrics = self.metrics + [{
            'MetricName': metric,
            'Dimensions': dimensions,
            'Unit': 'None',
            'Value': value
        }]

    def uploadMetrics(self):
        # Upload all metrics in a single call
        if len(self.metrics) > 0:
            logging.info("Uploading %d metrics" % len(self.metrics))
            for md in chunker(self.metrics, 20):
                res = self.cloudwatch.put_metric_data(
                    MetricData=md,
                    Namespace='ECSMonitor'
                )
                logging.info("Upload metrics chunk result: %s" % pformat(res))
        else:
            logging.info("No metrics to upload...?!")

    def scanServices(self, cluster):
        # Scan services running/desired (and optional availability metrics)
        paginator = self.ecs.get_paginator('list_services')
        serviceArns = []
        try:
            serviceArns = paginator.paginate(cluster=cluster).build_full_result()['serviceArns']
        except Exception as e:
            return self.error("Error fetching services on cluster {} - {}".format(cluster, pformat(e)))

        self.clusters[cluster]['services'] = {}
        for arns in chunker(serviceArns, 10):
            response = self.ecs.describe_services(
                cluster=cluster,
                services=arns
            )

            for service in response['services']:
                if self.includeServices is not None:
                    matched = False
                    for reg in self.includeServices:
                        if reg.search(service['serviceName']):
                            matched = True
                    if not matched:
                        logging.debug("Skipping service %s since it does not match supplied includeService regexes", service['serviceName'])
                        continue
                if self.excludeServices is not None:
                    for reg in self.excludeServices:
                        if reg.search(service['serviceName']):
                            logging.debug("Skipping serice %s since it matched an excludeServices regex", service['serviceName'])
                            continue
                logging.info("Cluster [%30.30s] Service[%10.10s] Status[%s] Desired[%s] Running[%s] Pending[%s]", cluster, service['serviceName'], service['status'], service['desiredCount'], service['runningCount'], service['pendingCount'])
                self.clusters[cluster]['services'][service['serviceName']] = service
                self.updateMetrics('Tasks_Running', service['runningCount'], [{'Name': 'ClusterName', 'Value': cluster}, {'Name': 'Service', 'Value': service['serviceName']}])
                self.updateMetrics('Tasks_Pending', service['pendingCount'], [{'Name': 'ClusterName', 'Value': cluster}, {'Name': 'Service', 'Value': service['serviceName']}])
                self.updateMetrics('Tasks_Desired', service['desiredCount'], [{'Name': 'ClusterName', 'Value': cluster}, {'Name': 'Service', 'Value': service['serviceName']}])
                # If tasks can not be placed/started they will not be pending. (instead, a log message will be thrown about insufficient resources or image can't be pulled etc)
                # Keep track of these, they should be 0 or need fixing (either through autoscaling or manual intervention):
                unstartable = service['desiredCount'] - service['runningCount'] - service['pendingCount']
                self.updateMetrics('Tasks_Unstartable', unstartable, [{'Name': 'ClusterName', 'Value': cluster}, {'Name': 'Service', 'Value': service['serviceName']}])
                # Next, if enabled, figure out scaling metrics -- how many tasks for this service still fit
                if self.scaleMetrics:
                    # if schedulingStrategy == 'REPLICA' only? Do we care about DAEMONs? -- TODO
                    # - instancesRequired: distinct instances needed - 0 if this is not a requirement, but if placementConstraint is distinctInstance this should reflect that
                    self.updateServiceScaleMetrics(cluster, service)

    def updateClusterScaleMetrics(self, clusterName):
        # We should have self.clusters[clusterName]['scaleMetrics'] available per service, parse those and produce clusterInstanceScale numbers
        # That single number should be negative if instances should be scaled in, 0 if all is ok, and positive if more instances are needed.
        # Note that a scaling bias is incorporated, with it you can specify the number of spare capacity you want to reserve, i.e. the number of times the biggest container needs to be added.
        # Alternatively when scaling bias can be set to 0 and scalingBiasCPU or scalingBiasMEM can be set, which require that amount of spare CPU/MEM
        # (both on the same instanec when both are set) to be available.
        if 'scaleMetrics' not in self.clusters[clusterName]:
            self.clusters[clusterName]['scaleMetrics'] = {}

        # So for this cluster we have a list of extra containers wanted per service. These already 'used' the currently available extra available space.
        # Let's sum those extra desired up and figure out how many extra instances we need.
        # First let's figure out the container size. We assume your instances are the same size, but we'll figure out the smallest to be safe.
        instanceCPU = 1024 * 1024 * 1024  # Just a big number that's unlikely to appear here
        instanceMEM = 1024 * 1024 * 1024  # Just a big number that's unlikely to appear here
        if not (clusterName in self.clusters and 'instances' in self.clusters[clusterName]):
            logging.error("This will die -    cluster %s looks like:\n%s", clusterName, pformat(self.clusters[clusterName]))
        if len(self.clusters[clusterName]['instances']) == 0:
            # Uh, we have no instances - we can't judge much from that.
            # We'll have to make a guess here, does not matter much, let's say 2048 CPU and 8GB for default. If your machines are bigger ... well, you might get extra instances for a while. Tough.
            instanceCPU = 2048
            instanceMEM = 8192
            logging.info("NOTICE: No instances in cluster %s, we're assuming %s CPU and %s memory for now!", clusterName, instanceCPU, instanceMEM)
        for instance in self.clusters[clusterName]['instances']:
            instance = self.clusters[clusterName]['instances'][instance]
            instance['registered_resources'] = {resource['name']: resource for resource in instance['registeredResources']}
            instance['remaining_resources'] = {resource['name']: resource for resource in instance['remainingResources']}
            if instance['registered_resources']['MEMORY']['integerValue'] < instanceMEM:
                instanceMEM = min(int(instanceMEM), int(instance['registered_resources']['MEMORY']['integerValue']))
            if instance['registered_resources']['CPU']['integerValue'] < instanceCPU:
                instanceCPU = min(int(instanceCPU), int(instance['registered_resources']['CPU']['integerValue']))

        logging.debug("Cluster %s scaling based on instances with %s CPU and %sMB memory.", clusterName, instanceCPU, instanceMEM)
        extraInstancesNeeded = 0

        scaleBiasCPUFilled = 0
        scaleBiasMEMFilled = 0

        # First fill the existing resources with services in a round-robin fashion.
        for instance in self.clusters[clusterName]['instances']:
            instance = self.clusters[clusterName]['instances'][instance]
            if instance['status'] != 'ACTIVE':
                # This one can't be used (anymore?) - skip it in this calculation
                continue
            # First fill the bias if needed / possible
            if scaleBiasCPUFilled < self.scaleBiasCPU or scaleBiasMEMFilled < self.scaleBiasMEM:
                # Does it fit?
                if instance['remaining_resources']['CPU']['integerValue'] > self.scaleBiasCPU and instance['remaining_resources']['MEMORY']['integerValue'] > self.scaleBiasMEM:
                    scaleBiasCPUFilled = self.scaleBiasCPU
                    scaleBiasMEMFilled = self.scaleBiasMEM
                    instance['remaining_resources']['CPU']['integerValue'] -= self.scaleBiasCPU
                    instance['remaining_resources']['MEMORY']['integerValue'] -= self.scaleBiasMEM
                    instance['pendingTasksCount'] += 1  # Pretend this is a task, otherwise it might get killed off

            for serviceName in self.clusters[clusterName]['scaleMetrics']:
                # While we want more containers and it fits, place it.
                if 'schedulingStrategy' not in self.clusters[clusterName]['services'][serviceName]:
                    # Fallback, sometimes it's missing... (i.e. old boto on AWS)
                    self.clusters[clusterName]['services'][serviceName]['schedulingStrategy'] = 'REPLICA'
                if self.clusters[clusterName]['services'][serviceName]['schedulingStrategy'] != 'REPLICA':
                    # Do not bother with DAEMON mode tasks for now. They can't run multiple times on one instance anyway.
                    continue
                while (self.clusters[clusterName]['scaleMetrics'][serviceName]['desiredExtraContainers'] > 0
                       and instance['remaining_resources']['CPU']['integerValue'] >= self.clusters[clusterName]['scaleMetrics'][serviceName]['cpuRequired']
                       and instance['registered_resources']['MEMORY']['integerValue'] >= self.clusters[clusterName]['scaleMetrics'][serviceName]['memRequired']
                       ):
                    # TODO -- add constraint if service needs distinctInstance etc
                    self.clusters[clusterName]['scaleMetrics'][serviceName]['desiredExtraContainers'] -= 1
                    instance['remaining_resources']['CPU']['integerValue'] -= self.clusters[clusterName]['scaleMetrics'][serviceName]['cpuRequired']
                    instance['registered_resources']['MEMORY']['integerValue'] -= self.clusters[clusterName]['scaleMetrics'][serviceName]['memRequired']
                    instance['pendingTasksCount'] += 1

        for serviceName in self.clusters[clusterName]['scaleMetrics']:
            # Figure out instances per service needed.
            # It's possible that one of these is 0, if that's the case we the other metric.
            if instanceCPU > 0:
                containersByCPU = int(instanceCPU / self.clusters[clusterName]['scaleMetrics'][serviceName]['cpuRequired']) if self.clusters[clusterName]['scaleMetrics'][serviceName]['cpuRequired'] > 0 else 128
            else:
                containersByCPU = 128
            if instanceMEM > 0:
                containersByMEM = int(instanceMEM / self.clusters[clusterName]['scaleMetrics'][serviceName]['memRequired']) if self.clusters[clusterName]['scaleMetrics'][serviceName]['memRequired'] > 0 else 128
            else:
                containersByMEM = 128

            containersPerInstance = min(containersByCPU, containersByMEM)

            if containersPerInstance > 0:
                changeCount = ceil(self.clusters[clusterName]['scaleMetrics'][serviceName]['desiredExtraContainers'] / float(containersPerInstance))
            else:
                if containersPerInstance == 0:
                    logging.warning("WARNING: divide of cpureq[%s] and memreq[%s] vs instanceCPU[%s]|instanceMEM[%s] -> containersByCPU[%s] / containersByMEM[%s] --> are you sure these instances are big enough for your tasks?!", self.clusters[clusterName]['scaleMetrics'][serviceName]['cpuRequired'], self.clusters[clusterName]['scaleMetrics'][serviceName]['memRequired'], instanceCPU, instanceMEM, containersByCPU, containersByMEM)
                logging.debug("ChangeCount based on 1 container per instance since our divide of cpureq[%s] and memreq[%s] vs instanceCPU[%s]|instanceMEM[%s] -> containersByCPU[%s] / containersByMEM[%s]", self.clusters[clusterName]['scaleMetrics'][serviceName]['cpuRequired'], self.clusters[clusterName]['scaleMetrics'][serviceName]['memRequired'], instanceCPU, instanceMEM, containersByCPU, containersByMEM)
                changeCount = int(self.clusters[clusterName]['scaleMetrics'][serviceName]['desiredExtraContainers'])
            logging.debug("Cluster %s Service %s, containersByCPU %s | containersByMEM %s -> containersPerInstance %s    vs desiredExtra %s -> changeCount %s", clusterName, serviceName, containersByCPU, containersByMEM, containersPerInstance, self.clusters[clusterName]['scaleMetrics'][serviceName]['desiredExtraContainers'], changeCount)

            if changeCount > 0:  # Scaling up is always done generously, otherwise it won't fit
                extraInstancesNeeded += ceil(changeCount)
            elif changeCount < 0:  # Scaling down is always done carefully -- we can't kill half a container.
                extraInstancesNeeded += floor(changeCount)
            # self.clusters[clusterName]['scaleMetrics'][serviceName]['changeCount'] = changeCount # Maybe push this as metric, probably overlaps though.

        # Let's see if we can scale down, since so far we only looked at -extra- needed. If we have 'idle' instances mark them for termination. (mind that scale bias has already been included in the services)
        if extraInstancesNeeded == 0:
            for instance in self.clusters[clusterName]['instances']:
                instance = self.clusters[clusterName]['instances'][instance]
                # Skip draining instances!
                if instance['status'] == 'ACTIVE' and instance['runningTasksCount'] == 0 and instance['pendingTasksCount'] == 0:
                    # Instance is idle!
                    logging.debug("Instance %s has 0 running and 0 pending tasks -> we can get rid of it.", instance['ec2InstanceId'])
                    extraInstancesNeeded -= 1

        # Make sure our reserved CPU/MEM is available now that the rest has been handled
        if scaleBiasCPUFilled < self.scaleBiasCPU or scaleBiasMEMFilled < self.scaleBiasMEM:
            biasInstancesNeeded = 0
            if self.scaleBiasMEM > 0:
                biasInstancesNeeded += (self.scaleBiasMEM / float(instanceMEM))
            if self.scaleBiasCPU > 0:
                biasInstancesNeeded += (self.scaleBiasCPU / float(instanceCPU))
            if biasInstancesNeeded > 0:
                logging.info("Adding %s instances due to CPU/MEM Scaling Bias (aka 'scaling space' - you want [%s/%sMB] extra based on [%s/%sMB] per instance)", ceil(biasInstancesNeeded), self.scaleBiasCPU, self.scaleBiasMEM, instanceCPU, instanceMEM)
                extraInstancesNeeded += ceil(biasInstancesNeeded)

        logging.debug("Cluster %s has clusterInstanceScale set to %s", clusterName, extraInstancesNeeded)
        self.updateMetrics('clusterInstanceScale', extraInstancesNeeded, [{'Name': 'ClusterName', 'Value': clusterName}])

    def updateServiceScaleMetrics(self, clusterName, service):
        # Figure out how many times this service can schedule itself given the current cluster availability.
        # I.e. if this task needs 1024 CPU and 4096 MB memory, how many times does it fit on the active instances
        # This info is stored in self.clusters[clusterName]['scaleMetrics']
        # Stored:
        # - desiredExtraContainers: number of extra times the container wants to be placed -- can be negative when desired is smaller than actual pending+running!
        # - cpuRequired: CPU needed per container
        # - memRequired: Mem needed per container
        # After grabbing all services there will be a magic number produced by updateClusterScaleMetrics() which can be used for scaling.

        # service = in self.clusters[clusterName]['services'][serviceName]:
        serviceName = service['serviceName']
        if service['launchType'] != 'EC2':
            logging.debug("Skipping %s service %s for scale metrics.", service['launchType'], serviceName)
            return

        td = self.getTaskDefinition(service['taskDefinition'])
        if 'containerDefinitions' not in td:
            logging.warning("Did not find containerDefs in taskdef %s (aka: taskdef fetch probably failed) for service %s - eject!", service['taskDefinition'], serviceName)
            return

        # Get mem and cpu needed.
        cpu = 0
        mem = 0
        for container_def in td['containerDefinitions']:
            if 'memory' in container_def:
                mem += int(container_def['memory'])
            elif 'memoryReservation' in container_def:
                mem += int(container_def['memoryReservation'])
            if 'cpu' in container_def:
                cpu += int(container_def['cpu'])

        desiredExtraContainers = service['desiredCount'] - service['runningCount'] - service['pendingCount'] + self.scaleBias

        if 'scaleMetrics' not in self.clusters[clusterName]:
            self.clusters[clusterName]['scaleMetrics'] = {}
        if serviceName not in self.clusters[clusterName]['scaleMetrics']:
            self.clusters[clusterName]['scaleMetrics'][serviceName] = {}
        self.clusters[clusterName]['scaleMetrics'][serviceName]['cpuRequired'] = int(cpu)
        self.clusters[clusterName]['scaleMetrics'][serviceName]['memRequired'] = int(mem)
        self.clusters[clusterName]['scaleMetrics'][serviceName]['desiredExtraContainers'] = int(desiredExtraContainers)

        # Make these available as metrics too
        self.updateMetrics('CPU_Required', cpu, [{'Name': 'ClusterName', 'Value': clusterName}, {'Name': 'Service', 'Value': serviceName}])
        self.updateMetrics('MEM_Required', mem, [{'Name': 'ClusterName', 'Value': clusterName}, {'Name': 'Service', 'Value': serviceName}])
        self.updateMetrics('desiredExtraContainers', desiredExtraContainers, [{'Name': 'ClusterName', 'Value': clusterName}, {'Name': 'Service', 'Value': serviceName}])

    def enableScaleMetrics(self, bias, biasCPU, biasMEM):
        self.scaleMetrics = True
        self.scaleBias = bias
        self.scaleBiasCPU = biasCPU
        self.scaleBiasMEM = biasMEM

    def handle(self, event, context):
        logging.info("ECS Monitor Lambda received the event {} with context {}".format(pformat(event), pformat(context)))
        self.event = event
        self.context = context
        self.result = {
            'success': 1,
            'msg': "Success",
        }

        # First see how we were called, was this Cloudwatch Event based or Cloudwatch Schedule based?
        if 'source' in event and event['source'] == 'aws.ecs':
            logging.info("Found aws.ecs as event source, let's see what happened.")
            if 'detail' in event and 'eventName' in event['detail']:
                logging.debug("Detail: event %s", event['detail']['eventName'])
                self.result['msg'] = "Event {} found.".format(event['detail']['eventName'])
            # TODO
            return self.result
        elif 'source' in event and event['source'] == 'aws.events':
            logging.debug("Triggered by schedule -- scanning mode")
            self.scanClusters()
            self.uploadMetrics()
            return self.result
        else:
            logging.warning("Could not handle event, key information missing!")
            return self.result

    def retrigger_lambda(self):
        time.sleep(random.uniform(2, 5))  # Wait between 2 and 5 secs
        try:
            if 'Reinvoked' in self.event:
                self.event['Reinvoked'] = self.event['Reinvoked'] + 1
                if self.event['Reinvoked'] > 300:
                    logger.warning("Reinvoke count 300 -- aborting this call!")
                    sys.exit(0)
            else:
                self.event['Reinvoked'] = 1
        except Exception as e:
            logging.warning("Error in reinvoke count checker: {}".format(pformat(e)))
            pass
        logging.warning("Re-triggering lambda function (reinvocation count {}) and exiting!".format(self.event['Reinvoked']))
        self.lam = boto3.client('lambda')
        self.lam.invoke(
            FunctionName=self.context.function_name,
            Payload=json.dumps(self.event),
            InvocationType='Event',
            # ClientContext='string',
        )
        sys.exit(0)


def lambda_handler(event, context):
    em = ECSMonitor()
    loglevel = os.environ['LogLevel'] if 'LogLevel' in os.environ else logging.INFO
    wantScaleMetrics = bool(os.environ['ScaleMetrics']) if 'ScaleMetrics' in os.environ else False
    scaleBias = int(os.environ['ScaleBias']) if 'ScaleBias' in os.environ else 0
    scaleBiasCPU = int(os.environ['ScaleBiasCPU']) if 'ScaleBiasCPU' in os.environ else 0
    scaleBiasMEM = int(os.environ['ScaleBiasMEM']) if 'ScaleBiasMEM' in os.environ else 0
    excludeClusters = os.environ['ExcludeClusters'] if 'ExcludeClusters' in os.environ else None
    excludeServices = os.environ['ExcludeServices'] if 'ExcludeServices' in os.environ else None
    includeClusters = os.environ['IncludeClusters'] if 'IncludeClusters' in os.environ else None
    includeServices = os.environ['IncludeServices'] if 'IncludeServices' in os.environ else None
    em.configure_logger(loglevel)
    em.setInclusions(includeClusters, includeServices, excludeClusters, excludeServices)
    if wantScaleMetrics:
        em.enableScaleMetrics(scaleBias, scaleBiasCPU, scaleBiasMEM)
    return em.handle(event, context)


# Used for cmdline testing
if __name__ == "__main__":
    e = dict(source='aws.events')
    c = dict()
    # Commandline call, Call lambda handler
    lambda_handler(e, c)
